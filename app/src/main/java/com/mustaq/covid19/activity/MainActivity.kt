package com.mustaq.covid19.activity

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatTextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.github.mikephil.charting.charts.BarChart
import com.github.mikephil.charting.components.Description
import com.github.mikephil.charting.components.Legend
import com.mustaq.covid19.R
import com.mustaq.covid19.custom.CustomProgressBar
import com.mustaq.covid19.model.AllStateModel
import com.mustaq.covid19.model.CovidAll
import com.mustaq.covid19.model.SingleStateModel

import com.mustaq.covid19.retrofitClient.RetrofitClientSingleton
import com.mustaq.covid19.utils.alertDialog
import com.mustaq.covid19.utils.isNetworkAvailable
import retrofit2.Call
import retrofit2.Response
import kotlin.collections.ArrayList
import kotlin.math.log


class MainActivity : AppCompatActivity() {
    lateinit var chart: BarChart
    var desc: Description? = null
    var legent: Legend? = null
    val xVals = ArrayList<String>()
    lateinit var tvCases: AppCompatTextView
    lateinit var tvActiveCases: AppCompatTextView
    lateinit var tvTotalDeath: AppCompatTextView
    lateinit var tvTotalRecovered: AppCompatTextView
    lateinit var tvTodayCase: AppCompatTextView
    lateinit var tvTodayRecovered: AppCompatTextView
    lateinit var customProgressBar: CustomProgressBar
    lateinit var constraintNoData: ConstraintLayout
    lateinit var constraintDataActivity: ConstraintLayout
    lateinit var stateName: String


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        customProgressBar = CustomProgressBar()
        stateName = "New York"
        initView()
    }

    private fun initView() {
        // chart = findViewById(R.id.chartLine)
        tvCases = findViewById(R.id.tvCases)
        tvActiveCases = findViewById(R.id.tvActiveCases)
        tvTotalDeath = findViewById(R.id.tvDeath)
        tvTotalRecovered = findViewById(R.id.tvTotalRecovered)
        tvTodayCase = findViewById(R.id.tvTodayCase)
        tvTodayRecovered = findViewById(R.id.tvTodayRecovered)
        constraintNoData = findViewById(R.id.no_data_layout);
        constraintDataActivity = findViewById(R.id.data_activity)



        if (isNetworkAvailable(this@MainActivity)) {
            //getall()
            //getDataFromAllState()
            getDataFromStateWise(stateName)

        } else {
            alertDialog(this, "Please check your network")
        }

    }

    private fun getDataFromStateWise(stateName: String) {
        RetrofitClientSingleton
            .getInstance()
            .apiGetDataFromStateWise(stateName)
            .enqueue(object : retrofit2.Callback<SingleStateModel> {
                override fun onFailure(call: Call<SingleStateModel>, t: Throwable) {
                    Log.e(TAG, "onFailure: $t")
                }

                override fun onResponse(
                    call: Call<SingleStateModel>,
                    response: Response<SingleStateModel>
                ) {
                    if (response.isSuccessful) {
                        Log.e(TAG, "onResponse: ")
                    } else {
                        Log.e(TAG, "onResponse Fail: ")
                    }
                }

            })
    }

    private fun getDataFromAllState() {
        RetrofitClientSingleton
            .getInstance()
            .apiGetDataFromAllState()
            .enqueue(object : retrofit2.Callback<List<AllStateModel>> {
                override fun onFailure(call: Call<List<AllStateModel>>, t: Throwable) {
                    Log.e(TAG, "onFailure: $t")
                }

                override fun onResponse(
                    call: Call<List<AllStateModel>>,
                    response: Response<List<AllStateModel>>
                ) {
                    if (response.isSuccessful) {
                        Log.e(TAG, "onResponse: ${response.body()}")
                    }
                }

            })
    }

    private fun getall() {
        customProgressBar.dialogShow(this@MainActivity)
        RetrofitClientSingleton
            .getInstance()
            .apiGetAllCaseForCovid()
            .enqueue(object : retrofit2.Callback<CovidAll> {
                override fun onFailure(call: Call<CovidAll>, t: Throwable) {
                    customProgressBar.hideDialog(this@MainActivity)
                    Log.e(TAG, "onFailure: $t")
                }

                @SuppressLint("SetTextI18n")
                override fun onResponse(call: Call<CovidAll>, response: Response<CovidAll>) {
                    if (response.isSuccessful) {
                        constraintDataActivity.visibility = View.VISIBLE
                        constraintNoData.visibility = View.GONE
                        val apiData = response.body()
                        tvCases.text =
                            getString(R.string.total_case) + returnIntToStringData(apiData!!.cases)
                        tvActiveCases.text =
                            getString(R.string.active_case) + returnIntToStringData(apiData.active)
                        tvTotalDeath.text =
                            getString(R.string.total_death) + returnIntToStringData(apiData.deaths)

                        tvTotalRecovered.text =
                            getString(R.string.total_recovered) + returnIntToStringData(apiData.recovered)
                        tvTodayCase.text =
                            getString(R.string.today_case) + returnIntToStringData(apiData.todayCases)
                        tvTodayRecovered.text =
                            getString(R.string.today_recovered) + returnIntToStringData(apiData.todayRecovered)
                        customProgressBar.hideDialog(this@MainActivity)
                    } else {
                        alertDialog(this@MainActivity, getString(R.string.data_error))
                        constraintNoData.visibility = View.VISIBLE
                        constraintDataActivity.visibility = View.GONE
                        customProgressBar.hideDialog(this@MainActivity)
                    }
                }

            })
    }

    companion object {
        const val TAG = "Main Activity"
    }

    fun customToast(context: Context, message: String) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }


    fun returnIntToStringData(intData: Int): String {
        return intData.toString()
    }

    fun returnDoubleToStringData(doubleData: Double): String {
        return doubleData.toString()
    }

}


/*

*@GET("api/uservaccinations/getvaccinationreport/{vaccination_id}/{user_id}")
Call<ViewVaccinationResponseModel> getVaccination(@Path("vaccination_id") String vaccination_id, @Path("user_id") String id);
**/