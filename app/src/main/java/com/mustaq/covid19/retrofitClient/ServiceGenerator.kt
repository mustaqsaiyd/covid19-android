package com.mustaq.covid19.retrofitClient

import com.mustaq.covid19.model.AllStateModel
import com.mustaq.covid19.model.CovidAll
import com.mustaq.covid19.model.ListWishAllState
import com.mustaq.covid19.model.SingleStateModel
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.QueryMap


interface ServiceGenerator {

    @GET(ALL)
    fun apiGetAllCaseForCovid(): Call<CovidAll>

    @GET("states/{state}")
    fun apiGetDataFromStateWise(@Path("state") state: String): Call<SingleStateModel>

    /**@GET("api/uservaccinations/getvaccinationreport/{vaccination_id}/{user_id}")
    Call<ViewVaccinationResponseModel> getVaccination(@Path("vaccination_id")
    String vaccination_id, @Path("user_id") String id);**/


    @GET(ALLSTATE)
    fun apiGetDataFromAllState(): Call<List<AllStateModel>>

}