package com.mustaq.covid19.retrofitClient


import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit


class RetrofitClientSingleton {

    companion object {
        val TAG = RetrofitClientSingleton::class.java.simpleName
        var retrofit: Retrofit? = null
        fun getInstance(): ServiceGenerator {
            if (retrofit == null) {
                val loggingInterceptor = HttpLoggingInterceptor()
                loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
                val okHttpBuilder = OkHttpClient.Builder()
                okHttpBuilder.readTimeout(5, TimeUnit.MINUTES)
                okHttpBuilder.connectTimeout(5, TimeUnit.MINUTES)
                okHttpBuilder.writeTimeout(5, TimeUnit.MINUTES)

                val client = okHttpBuilder.addInterceptor(loggingInterceptor).build()
//                client.networkInterceptors.add

                retrofit = Retrofit.Builder().baseUrl(BASEURL)


                    .addConverterFactory(
                        GsonConverterFactory.create(
                            GsonBuilder()
                                .setLenient()
                                .create()
                        )
                    )
                    .client(client)
                    .build()
            }

            return retrofit!!.create(ServiceGenerator::class.java)
        }

    }

}