package com.mustaq.covid19.utils

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.ConnectivityManager
import android.view.Window
import com.mustaq.covid19.R
import kotlinx.android.synthetic.main.alart_layout.*

fun alertDialog(context: Context, message: String) {
    val myDialog = Dialog(context)
    myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
    myDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
    myDialog.setContentView(R.layout.alart_layout)
    myDialog.tvAlertTitle.text = message
    myDialog.show()

    myDialog.btn_ok.setOnClickListener {
        myDialog.dismiss()
    }
}

fun isNetworkAvailable(context: Context): Boolean {
    val connectivityManager =
        context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    val activeNetworkInfo = connectivityManager.activeNetworkInfo
    return activeNetworkInfo != null && activeNetworkInfo.isConnected
}
