package com.mustaq.covid19.model

class CovidAll {
    /**
     * updated : 1594363230551
     * cases : 12395417
     * todayCases : 16158
     * deaths : 557512
     * todayDeaths : 903
     * recovered : 7225209
     * todayRecovered : 42270
     * active : 4612696
     * critical : 58654
     * casesPerOneMillion : 1590
     * deathsPerOneMillion : 71.5
     * tests : 264755864
     * testsPerOneMillion : 34117.7
     * population : 7760074632
     * oneCasePerPeople : 0
     * oneDeathPerPeople : 0
     * oneTestPerPeople : 0
     * activePerOneMillion : 594.41
     * recoveredPerOneMillion : 931.07
     * criticalPerOneMillion : 7.56
     * affectedCountries : 215
     */
    var updated: Long = 0
    var cases = 0
    var todayCases = 0
    var deaths = 0
    var todayDeaths = 0
    var recovered = 0
    var todayRecovered = 0
    var active = 0
    var critical = 0
    var casesPerOneMillion = 0
    var deathsPerOneMillion = 0.0
    var tests = 0
    var testsPerOneMillion = 0.0
    var population: Long = 0
    var oneCasePerPeople = 0
    var oneDeathPerPeople = 0
    var oneTestPerPeople = 0
    var activePerOneMillion = 0.0
    var recoveredPerOneMillion = 0.0
    var criticalPerOneMillion = 0.0
    var affectedCountries = 0

}