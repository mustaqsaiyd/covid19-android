package com.mustaq.covid19.model

class AllStateModel {
        /**
         * state : New York
         * updated : 1594382136770
         * cases : 425072
         * todayCases : 0
         * deaths : 32343
         * todayDeaths : 0
         * active : 244159
         * casesPerOneMillion : 21851
         * deathsPerOneMillion : 1663
         * tests : 4468203
         * testsPerOneMillion : 229686
         */
        var state: String? = null
        var updated: Long = 0
        var cases = 0
        var todayCases = 0
        var deaths = 0
        var todayDeaths = 0
        var active = 0
        var casesPerOneMillion = 0
        var deathsPerOneMillion = 0
        var tests = 0
        var testsPerOneMillion = 0

    }