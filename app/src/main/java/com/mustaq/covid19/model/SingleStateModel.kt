package com.mustaq.covid19.model

class SingleStateModel {
    /**
     * state : string
     * updated : 0
     * cases : string
     * todayCases : string
     * deaths : string
     * todayDeaths : string
     * active : string
     * casesPerOneMillion : string
     * deathsPerOneMillion : string
     * tests : string
     * testsPerOneMillion : string
     */
    var state: String? = null
    var updated = 0
    var cases: String? = null
    var todayCases: String? = null
    var deaths: String? = null
    var todayDeaths: String? = null
    var active: String? = null
    var casesPerOneMillion: String? = null
    var deathsPerOneMillion: String? = null
    var tests: String? = null
    var testsPerOneMillion: String? = null

}